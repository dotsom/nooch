<?php
/*
Template Name: How It Works
*/
?>

<?php get_header(); ?>
<div class="how-wrap page-baseline">
	<div class="head-wrap about-page">
		<h1 class="section-header how-header mobile-only">How It Works</h1>
		<h1 class="section-header how-header tab-desk-only">About Nooch</h1>
		<nav class="about-subnav tab-desk-only">
			<ul>	
				<li><a href="<?php echo get_page_link(1825); ?>" class="sub-nav-links">Overview</a></li>
				<li><a href="#" class="sub-nav-links selected">How It Works</a></li>
				<li><a href="<?php echo get_page_link(1826); ?>" class="sub-nav-links">Security</a></li>
				<li><a href="<?php echo get_page_link(1827); ?>" class="sub-nav-links">Team</a></li>
			</ul>
		</nav>
	</div>
	<ul id="how-buckets">
		
		<?php
$how_args = array("post_type" => "how_it_works", "posts_per_page" => -1);
$how_query = new WP_Query( $how_args );
$how_count = 0;

while ( $how_query->have_posts() ) : $how_query->the_post(); 
	$post_id = get_the_ID();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	?>

	<li id="how-bucket-<?php if( $how_count == 0 ) {
		echo "text";
	} else {
		echo $how_count;
	} ?>" style="background-image: url('<?php echo $image[0]; ?>')">

	<?php
	if ( $how_count > 0 ) {
		echo '<div class="how-dividing-line"></div>';
	} 
	?>
		<?php the_content(); ?>
	</li>
<?php
	$how_count++;
endwhile;
?>

	</ul>
</div>
<?php get_footer(); ?>
