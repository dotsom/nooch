<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="single-wrap eightcol first clearfix" role="main">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article class="blog-article" id="post-<?php the_ID(); ?>">
        						<header class="entry-header">
            						<img alt="<?php the_author_meta('nickname'); ?>" class="tab-desk-only team-profile-pic" src="<?php echo get_template_directory_uri(); ?>/library/images/teampics/<?php the_author_meta('nickname')?>.png" />
            						<h2><?php the_title(); ?></h2>
        						</header>

        						<?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'mobile-image', NULL, "mobile-image-600"); endif; ?>

        						<a class="featured-image"href="<?php echo get_permalink(); ?>"><img class="feat-img" width="744" height="300" src="<?php $img=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID)); echo $img[0]; ?>" alt="<?php the_title(); ?>"/></a>

        						<div class='text-wrapper'>
        							<p class="byline vcard"><?php
										printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), bones_get_the_author_posts_link());
									?></p>

            						<?php the_content() ?>
            					</div>
            					<footer class="article-footer">
									<?php the_tags('<p class="tags"><span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', '</p>'); ?>
								</footer>
    						</article>

						<?php endwhile; ?>
						<?php endif; ?>
						<?php comments_template(); ?>

					</div> <!-- end #main -->

					<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->
			</div>

<?php get_footer(); ?>
