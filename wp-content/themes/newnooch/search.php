<?php get_header(); ?>

			<div id="content">

				<div class="outer-wrap">

					<div class="head-wrap blog-page">
        							<h1 class="section-header blog-header tab-desk-only"><span><?php _e('Search Results for:', 'bonestheme'); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>
    							</div>
    							<div class="head-wrap about-page mobile-only">
        							<h1 class="section-header blog-header mobile-only"><span><?php _e('Search Results for:', 'bonestheme'); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>
    							</div>

						<div class="blog-wrap page-baseline">


						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article class="blog-article" id="post-<?php the_ID(); ?>">

        						<header class="entry-header">
            					<img alt="<?php the_author_meta(nickname); ?>" class="tab-desk-only team-profile-pic" src="<?php echo get_template_directory_uri(); ?>/library/images/teampics/<?php the_author_meta(nickname)?>.png" />
            					<a class='entry-header-link' href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
        					</header>

        					<?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'mobile-image', NULL, "mobile-image-600"); endif; ?>

        					<?php if ( has_post_thumbnail() ) { ?>
        					<a class="featured-image"href="<?php echo get_permalink(); ?>"><img class="feat-img" width="744" height="300" src="<?php $img=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID)); echo $img[0]; ?>" alt="<?php the_title(); ?>"/></a>
        					<?php } ?>

        					<div class='text-wrapper'>
            					<?php the_excerpt(); ?>
							</div>
    						</article>

						<?php endwhile; ?>

								<?php if (function_exists('bones_page_navi')) { ?>
										<?php bones_page_navi(); ?>
								<?php } else { ?>
										<nav class="wp-prev-next">
												<ul class="clearfix">
													<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
													<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
												</ul>
										</nav>
								<?php } ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<section class="entry-content">
											<p><?php _e("Sorry, I didn't find anything for that search you searched.  You could try searching again, but what you're searching for isn't behind this door...", "bonestheme"); ?></p>
										</section>
									</article>

							<?php endif; ?>

						</div> <!-- end #main -->

							<?php get_sidebar(); ?>

					</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
