<?php
/*
Template Name: Contact Us Backend
*/
?>

<?php
/*
This php doc formats the fields found at www.nooch.com/contact_us and sends an email to cliff
containing the message

By Tom
*/    

/*Get the name, email address, and message body, filter them, and assign them to variables*/
$field_name = filter_var( $_POST['cf_name'], FILTER_SANITIZE_STRING);

$field_email = filter_var( $_POST['cf_email'], FILTER_SANITIZE_EMAIL);

$field_message = filter_var( $_POST['cf_comment'], FILTER_SANITIZE_STRING);

/*Assigns cliff's email to $mail_to, to be used in the mail() function*/
$mail_to = 'tom@nooch.com';

/*Assigns the following to the $subject variable, to be used in the mail() function*/
$subject = 'Message from a site visitor: '.$field_name;

/*Creates a variable, $body_message, that contains name, email address, and message entered on the site
to be used in the mail() function*/
$body_message = 'From: '.$field_name."\n";
$body_message .= 'E-mail: '.$field_email."\n";
$body_message .= 'Message: '.$field_message;

/*Creates the variable $headers, which tells us who this is from and to where we will reply*/
$headers = 'From: '.$field_email."\r\n";
$headers .= 'Reply-To: '.$field_email."\r\n";

/*Send the email using all of the previous info, return a boolian value depending on whether the email was sent successfully*/
$mail_status = mail($mail_to, $subject, $body_message, $headers);

/*Displays a message depending on the value of $mail_status*/
if ($mail_status) { ?>
	<script language="javascript" type="text/javascript">
		alert("Thanks for contacting us at Nooch.  We'll do our best to get back to you as soon as humanly possible.  This is your info.");
		window.location = '/';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		alert('Uh oh, something went wrong! Please send an email to contact@nooch.com.  Sorry about that!');
		window.location = '/';
	</script>
<?php
}
?>