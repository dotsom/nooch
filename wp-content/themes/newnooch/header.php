<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!--<script src="<?php echo get_template_directory_uri(); ?>/library/js/shadowbox.js"></script>
		<script type="text/javascript">Shadowbox.init();</script>-->

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		<!-- drop Google Analytics Here -->
		<!-- end analytics -->
	</head>
	<body <?php body_class(); ?>>
		<div id="container">
			<header class="header" id="page-header" role="banner">
				<a id="logo" href="/"></a>
				<div id="hamb-wrap" class="float-right">
					<div class="hamb-stripe" id="hamb-top"></div>
					<div class="hamb-stripe" id="hamb-mid"></div>
					<div class="hamb-stripe" id="hamb-bot"></div>
				</div>

				<nav id="header-nav" class="tab-desk-only">
					<ul>
						<li><a href="<?php echo get_page_link(1825); ?>">About</a></li>
						<li><a href="<?php echo get_page_link(1821); ?>">Help</a></li>
						<li><a href="<?php echo get_page_link(1819); ?>">Blog</a></li>
						<li><a href="<?php echo get_page_link(1830); ?>" class="green">Request</a></li>
					</ul>
				</nav>

			</header> <!-- end header -->
			<nav id="header-nav" class="mobile-nav">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="<?php echo get_page_link(1823); ?>">About</a></li>
					<li><a href="<?php echo get_page_link(1821); ?>">Help</a></li>
					<li><a href="<?php echo get_page_link(1819); ?>">Blog</a></li>
					<li><a href="<?php echo get_page_link(1830); ?>">Request</a></li>
				</ul>
			</nav>