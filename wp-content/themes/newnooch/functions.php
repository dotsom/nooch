<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once('library/admin.php'); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once('library/translation/translation.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'team-img-size', 150, 300, true );
add_image_size( 'mobile-image-600', 600, 600);
add_image_size( 'blog-image-744', 744, 304);
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'bonestheme'),
		'description' => __('The first (primary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar( array(
		'name' => 'Footer Widget',
		'id' => 'footer-widget',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'bonestheme'),
		'description' => __('The second (secondary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__('<cite class="fn">%s</cite>', 'bonestheme'), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__('F jS, Y', 'bonestheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'bonestheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_pageurl( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','bonestheme').'" />
	<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
	</form>';
	return $form;
} // don't remove this bracket!


add_action('wp_enqueue_scripts', 'ls_scripts');
function ls_scripts() {
	if(!is_admin()) {
	wp_enqueue_script('jquery-easing', get_template_directory_uri() . '/library/js/jquery.easing.1.3.js', array('jquery'), '', true );
	wp_enqueue_script('jquery-touchSwipe', get_template_directory_uri() . '/library/js/jquery.touchSwipe.min.js', array('jquery-easing'), '', true );
	wp_enqueue_script('jquery-ls', get_template_directory_uri() . '/library/js/jquery.liquid-slider.min.js', array('jquery-touchSwipe'), '', true );
	wp_enqueue_script('shadowbox', get_template_directory_uri() . '/library/js/shadowbox.js', array('jquery'), '', true );
	
	}
}

function wp_pagenavi() {
  global $wp_query, $wp_rewrite;
  $pages = '';
  $max = $wp_query->max_num_pages;
  if (!$current = get_query_var('paged')) $current = 1;
  $args['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
  $args['total'] = $max;
  $args['current'] = $current;
 
  $total = 1;
  $args['mid_size'] = 3;
  $args['end_size'] = 1;
  $args['prev_text'] = '«';
  $args['next_text'] = '»';
 
  if ($max > 1) echo '</pre>
<div class="wp-pagenavi"><span class="wp-pagenavi-center">';
 if ($total == 1 && $max > 1) $pages = '<span class="pages">Page ' . $current . ' of ' . $max . '</span>';
 echo $pages . paginate_links($args);
 if ($max > 1) echo '</span></div>
<pre>';
}

if (class_exists('MultiPostThumbnails')) {
        new MultiPostThumbnails(
            array(
                'label' => 'Mobile Featured Image',
                'id' => 'mobile-image',
                'post_type' => 'post'
            )
        );
    }

//Post types

add_action( 'init', 'register_cpt_home_page' );

function register_cpt_home_page() {

    $labels = array( 
        'name' => _x( 'Home Page', 'home_page' ),
        'singular_name' => _x( 'Home Page ', 'home_page' ),
        'add_new' => _x( 'Add New', 'home_page' ),
        'add_new_item' => _x( 'Add New Home Page ', 'home_page' ),
        'edit_item' => _x( 'Edit Home Page ', 'home_page' ),
        'new_item' => _x( 'New Home Page ', 'home_page' ),
        'view_item' => _x( 'View Home Page ', 'home_page' ),
        'search_items' => _x( 'Search Home Page', 'home_page' ),
        'not_found' => _x( 'No Home Page found', 'home_page' ),
        'not_found_in_trash' => _x( 'No Home Page found in Trash', 'home_page' ),
        'parent_item_colon' => _x( 'Parent Home Page :', 'home_page' ),
        'menu_name' => _x( 'Home Page', 'home_page' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies' => array('category'),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'home_page', $args );
}

add_action( 'init', 'register_cpt_security_blurb' );

function register_cpt_security_blurb() {

    $labels = array( 
        'name' => _x( 'Security', 'security_blurb' ),
        'singular_name' => _x( 'Security', 'security_blurb' ),
        'add_new' => _x( 'Add New', 'security_blurb' ),
        'add_new_item' => _x( 'Add New Security', 'security_blurb' ),
        'edit_item' => _x( 'Edit Security', 'security_blurb' ),
        'new_item' => _x( 'New Security', 'security_blurb' ),
        'view_item' => _x( 'View Security', 'security_blurb' ),
        'search_items' => _x( 'Search Security', 'security_blurb' ),
        'not_found' => _x( 'No Security found', 'security_blurb' ),
        'not_found_in_trash' => _x( 'No Security found in Trash', 'security_blurb' ),
        'parent_item_colon' => _x( 'Parent Security:', 'security_blurb' ),
        'menu_name' => _x( 'Security', 'security_blurb' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 8,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'security_blurb', $args );
}

add_action( 'init', 'register_cpt_team_bio' );

function register_cpt_team_bio() {

    $labels = array( 
        'name' => _x( 'Team Bios', 'team_bio' ),
        'singular_name' => _x( 'Team Bio', 'team_bio' ),
        'add_new' => _x( 'Add New', 'team_bio' ),
        'add_new_item' => _x( 'Add New Team Bio', 'team_bio' ),
        'edit_item' => _x( 'Edit Team Bio', 'team_bio' ),
        'new_item' => _x( 'New Team Bio', 'team_bio' ),
        'view_item' => _x( 'View Team Bio', 'team_bio' ),
        'search_items' => _x( 'Search Team Bios', 'team_bio' ),
        'not_found' => _x( 'No team bios found', 'team_bio' ),
        'not_found_in_trash' => _x( 'No team bios found in Trash', 'team_bio' ),
        'parent_item_colon' => _x( 'Parent Team Bio:', 'team_bio' ),
        'menu_name' => _x( 'Team Bios', 'team_bio' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 7,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'team_bio', $args );
}



add_action( 'init', 'register_cpt_how_it_works' );

function register_cpt_how_it_works() {

    $labels = array( 
        'name' => _x( 'How It Works', 'how_it_works' ),
        'singular_name' => _x( 'How It Works', 'how_it_works' ),
        'add_new' => _x( 'Add New', 'how_it_works' ),
        'add_new_item' => _x( 'Add New How It Works', 'how_it_works' ),
        'edit_item' => _x( 'Edit How It Works', 'how_it_works' ),
        'new_item' => _x( 'New How It Works', 'how_it_works' ),
        'view_item' => _x( 'View How It Works', 'how_it_works' ),
        'search_items' => _x( 'Search How It Works', 'how_it_works' ),
        'not_found' => _x( 'No How It Works found', 'how_it_works' ),
        'not_found_in_trash' => _x( 'No How It Works found in Trash', 'how_it_works' ),
        'parent_item_colon' => _x( 'Parent How It Works:', 'how_it_works' ),
        'menu_name' => _x( 'How It Works', 'how_it_works' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 6,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'how_it_works', $args );
}

add_action( 'init', 'register_cpt_np_post' );

function register_cpt_np_post() {

    $labels = array( 
        'name' => _x( 'Non-Profit Blurb', 'np_post' ),
        'singular_name' => _x( 'Non-Profit Blurb', 'np_post' ),
        'add_new' => _x( 'Add New', 'np_post' ),
        'add_new_item' => _x( 'Add New Non-Profit Blurb', 'np_post' ),
        'edit_item' => _x( 'Edit Non-Profit Blurb', 'np_post' ),
        'new_item' => _x( 'New Non-Profit Blurb', 'np_post' ),
        'view_item' => _x( 'View Non-Profit Blurb', 'np_post' ),
        'search_items' => _x( 'Search Non-Profit Blurb', 'np_post' ),
        'not_found' => _x( 'No Non-Profit Blurb found', 'np_post' ),
        'not_found_in_trash' => _x( 'No Non-Profit Blurb found in Trash', 'np_post' ),
        'parent_item_colon' => _x( 'Parent Non-Profit Blurb:', 'np_post' ),
        'menu_name' => _x( 'Non-Profit Blurb', 'np_post' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 8,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'np_post', $args );
}
?>
