<?php
/*
Template Name: Overview
*/
?>

<?php get_header(); ?>
<div class="overview-wrap page-baseline">
	<div class="head-wrap about-page">
		<h1 class="section-header overview-header tab-desk-only">About Nooch</h1>
		<nav class="about-subnav tab-desk-only">
			<ul>	
				<li><a href="#" class="sub-nav-links selected">Overview</a></li>
				<li><a href="<?php echo get_page_link(1823); ?>" class="sub-nav-links">How It Works</a></li>
				<li><a href="<?php echo get_page_link(1826); ?>" class="sub-nav-links">Security</a></li>
				<li><a href="<?php echo get_page_link(1827); ?>" class="sub-nav-links">Team</a></li>
			</ul>
		</nav>
	</div>
	<div class="about-content">
        <div id="vid-container" class="tab-desk-only">
            <iframe id="vid-demo" src="https://player.vimeo.com/video/63765945?autoplay=1&loop=1&title=0&byline=0&portrait=0" height="360" width="240" frameborder="0"></iframe>
        </div>
        <div id="bucket1">
            <!--Bucket 1-->
            <h1 id="bucket1h1">Money</h1>
            <h2 id="bucket1h2">Made Simple</h2>
        	<a id="overview-play-button" class="mobile-only" href="https://player.vimeo.com/video/63765945?autoplay=1&loop=1&title=0&byline=0&portrait=0"><img height="77" width="60" src="<?php echo get_template_directory_uri(); ?>/library/images/watch-demo.png"></a><!--
         --><p id="bucket1p">Nooch is the quickest, easiest way to pay.
                            Payments take less than 30 seconds, and
	    					can be made any time, anywhere.  Whether it's
		    				for PECO or pizza, you can settle up instantly.</p>
            </div>
        <!--Bucket 2-->
        <h2 id="bucket2h2">Power in Names</h2>
        <p id="bucket2p">To find friends on Nooch, you only need
                        to know their names.  We use Facebook
						and phonebook connectivity to make finding
                        your friends as simple as it should be.</p>
        <!--Bucket 3-->
        <h2 id="bucket3h2">Safety and Security</h2>
        <p id="bucket3p">Your Nooch account is protected by
                        bank grade encryption, a secure PIN, and
                        geolocation technology. If you feel
                        that a transfer has been made without
                        your approval, we’ll fix it.</p>
        <!--Bucket 4-->
        <h2 id="bucket4h2">Want to Learn More?</h2>
        <p id="bucket4p">You can check out our
                        faq <a class="stdlink" href="<?php echo get_page_link(/*FAQ*/); ?>">here</a> or shoot us
                        an email if you’d like at
                        <a class="stdlink" href="mailto:info@nooch.com">info@nooch.com</a></p>
    </div>
</div>
<?php get_footer(); ?>
