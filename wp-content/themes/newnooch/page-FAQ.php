<?php
/*
Template Name: FAQ
*/
?>

<?php get_header(); ?>
<div class="faq-wrap page-baseline">
	<div class="head-wrap help-page">
		<h1 class="section-header faq-header mobile-only">Frequently Asked Questions</h1>
		<h1 class="section-header faq-header tab-desk-only">Nooch Support</h1>
		<nav class="help-subnav tab-desk-only">
			<ul>	
				<li><a href="<?php echo get_page_link(1821); ?>" class="sub-nav-links">Contact Us</a></li>
				<li><a href="#" class="sub-nav-links selected">FAQ</a></li>
				<li><a href="http://support.nooch.com" target="_blank" class="sub-nav-links">Support Center</a></li>
			</ul>
		</nav>
	</div>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article id="faq-article">
		<div class="exp-cont-wrap">
			<a href="#" id="expand-all" class="button actionable" style="font-size: 12px;">Expand All</a>
			<a href="#" id="collapse-all" class="button" style="font-size: 12px;">Collapse All</a>
		</div>
		<?php the_content(); ?>
		<a href="http://support.nooch.com" class="more-help" target="_blank" >View more help topics ></a>
	</article>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>