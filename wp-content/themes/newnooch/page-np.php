<?php
/*
Template Name: Non Profit
*/
?>

<?php get_header(); ?>

<div class="secure-wrap page-baseline">
	<div class="head-wrap about-page">
		<?php
$page_args = array("page_id" => 398 /*production id is 1826*/);
$page_query = new WP_Query( $page_args );

while ( $page_query->have_posts() ) : $page_query->the_post();
	$page_id = get_the_ID();
	$page_image = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'single-post-thumbnail' );
?>
	<h1 class="section-header secure-header mobile-only"><?php the_title(); ?></h1>
		<h1 class="section-header secure-header tab-desk-only"><?php the_title(); ?></h1>
<?php endwhile;
?>
		<nav class="about-subnav tab-desk-only">
			<ul>	
				<li><a href="<?php echo get_page_link(); ?>" class="sub-nav-links">Overview</a></li>
				<li><a href="<?php echo get_page_link(); ?>" class="sub-nav-links">How It Works</a></li>
				<li><a href="<?php echo get_page_link(); ?>" class="sub-nav-links">Security</a></li>
				<li><a href="<?php echo get_page_link(); ?>" class="sub-nav-links">Team</a></li>
			</ul>
		</nav>
	</div>
<?php
$page_args = array("page_id" => 398 /*production id is 1826*/);
$page_query = new WP_Query( $page_args );

while ( $page_query->have_posts() ) : $page_query->the_post();
	$page_id = get_the_ID();
	$page_image = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'single-post-thumbnail' );
?>
	<ul id="secure-buckets" style="background-image: url('<?php echo $page_image[0]; ?>')">
<?php
endwhile;
?>

<?php
$security_args = array("post_type" => "np_post", "posts_per_page" => -1);
$security_query = new WP_Query( $security_args );
$security_count = 1;

while ( $security_query->have_posts() ) : $security_query->the_post(); 
	$post_id = get_the_ID();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	
?>
	<li class="safe-bucket-wrap" id="safe-bucket-<?php echo $security_count; ?>" style="background-image: url('<?php echo $image[0]; ?>')">
		<?php the_content(); ?>
	</li>
<?php
	$security_count++;
endwhile;
?>

	</ul>
</div>

<?php get_footer(); ?>

<!--1826-->