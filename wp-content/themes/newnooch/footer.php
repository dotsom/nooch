			    <div class="footer-clear"></div>
            </div> <!-- end #container -->
            <footer class="footer" role="contentinfo">
				<div id="inner-footer">
					<!--Mailchimp quick sign up-->
                    <nav id="footer-nav"><!--
                    --><form action="http://nooch.us4.list-manage.com/subscribe/post?u=7f2f23cb3666b5cf4efafcef8&id=ca001f66ce"
                        method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" 
                        class="validate footer-form" target="_blank" novalidate>
                        <h3>Stay in the loop!</h3>
                        <p> Be the first to know when Nooch goes live!</p>
                        <input type="email" value="Email*" name="EMAIL" class="email" id="mce-EMAIL" onfocus="if(this.value == 'Email*') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Email*'; }" value="Email*"><!--
                     --><input value="Submit" id="footer-submit" type="submit">
                        </form><!--

                     --><ul id="about-list" class="footer-list">
                			<li><h6>About</h6></li>
                			<li><a class="footerlinks" href="<?php echo get_page_link(1825); ?>">Overview</a></li>
                			<li><a class="footerlinks" href="<?php echo get_page_link(1823); ?>">How It Works</a></li>
                			<li><a class="footerlinks" href="<?php echo get_page_link(1826); ?>">Security</a></li>
                			<li><a class="footerlinks" href="<?php echo get_page_link(1827); ?>">Team</a></li>
                			<li><a class="lastlink" href="<?php echo get_page_link(1819); ?>">Blog</a></li>
                		</ul><!--
                     --><ul id="help-list" class="footer-list">
                			<li><h6>Help</h6></li>
                			<li><a class="footerlinks" href="http://support.nooch.com/">Support Center</a></li>
                			<li><a class="footerlinks" href="<?php echo get_page_link(1822); ?>">FAQ</a></li>
                			<li><a class="lastlink" href="<?php echo get_page_link(1821); ?>">Contact Us</a></li>
                		</ul><!--
                     --><ul id="social-list" class="footer-list">
                			<li id='social-header'><h6>Social</h6></li>
                			<li><a id="fb-link" class="footerlinks" target="_blank" href="https://www.facebook.com/NoochMoney">Facebook</a></li>
                			<li><a id="tw-link" class="footerlinks" target="_blank" href="https://twitter.com/NoochMoney">Twitter</a></li>
                			<li><a id="in-link" class="footerlinks" target="_blank" href="https://instagram.com/noochmoney">Instagram</a></li>
                			<li><a id="gp-link" class="lastlink" target="_blank" href="https://plus.google.com/103970776778749256696/posts">Google +</a></li>
                		</ul><!--
        		     --><div id="twitter-div" class="desk-only">
                            <ul id="twitter_update_list"><?php
                                if(is_active_sidebar('footer-widget')){
                                    dynamic_sidebar('footer-widget');
                                }
                                ?>
                            </ul>
                        </div>
                    </nav>
				</div>
			</footer>
			<div id="footerbottom">
    			<div id="footleft" class="colorfoot"><p class="copyright" class="desk-only">Copyright © 2013 Nooch Inc. All Rights Reserved.</p></div><!--
    		 --><div id="footcenter1" class="desk-only"></div><!--
    		 --><div id="footcenter" class="colorfoot"></div><!--
    		 --><div id="footcenter2" class="desk-only"></div><!--
    		 --><div id="footright" class="colorfoot"></div><!--
		 --></div>
		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page. what a ride! -->
