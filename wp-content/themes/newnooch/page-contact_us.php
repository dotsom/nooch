<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>
<div class="contact-wrap page-baseline">
	<div class="head-wrap help-page">
		<h1 class="section-header contact-header mobile-only">Contact Us</h1>
		<h1 class="section-header contact-header tab-desk-only">Nooch Support</h1>
		<nav class="help-subnav tab-desk-only">
			<ul>	
				<li><a href="#" class="sub-nav-links selected">Contact Us</a></li>
				<li><a href="<?php echo get_page_link(1822); ?>" class="sub-nav-links">FAQ</a></li>
				<li><a href="http://support.nooch.com" target="_blank" class="sub-nav-links">Support Center</a></li>
			</ul>
		</nav>
	</div>
	<div id='contact-content-wrapper'>
	<div class="first-third">
		<a href="http://support.nooch.com" target="_blank">Go to our support center</a>
		<a href="#" target="_blank">855 269 6662</a>
		<a href="mailto:support@nooch.com" target="_blank">Support@Nooch.com</a>
	</div>
	<div class="second-third">
		<h2>Drop Us a Line</h2>
		<small>required fields are marked with <span>*</span></small>
		<form action="http://localhost:8080/nooch-contact-us-backend-sender" method="post">
        	<input id="name" name="cf_name" type="text" class="name" placeholder="*name"/>

			<input id="email" name="cf_email" type="text" class="email" placeholder="*email"/>

        	<textarea id="comment" name="cf_comment" type="text" class="comment" placeholder="We're all ears..."></textarea>
           
            <input id="contactus-submit" type="submit">
        </form>
	</div>
	<div class="third-third tab-desk-only">
		<ul>
			<li><a target="_blank" href="https://www.facebook.com/NoochMoney">facebook</a></li>
			<li><a target="_blank" href="https://twitter.com/NoochMoney">twitter</a></li>
			<li><a target="_blank" href="https://plus.google.com/103970776778749256696/posts">google +</a></li>
		</ul>
	</div>
</div>
	
</div>
<?php get_footer(); ?>