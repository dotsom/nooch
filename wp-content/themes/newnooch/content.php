<article id="post-<?php the_ID(); ?>" class="blog-post">
    <header class="entry-header">
        <img alt="<?php the_author_meta(nickname); ?>" class="team-profile-pic" src="https://www.nooch.com/00Images/teampics/<?php the_author_meta(nickname)?>.png" />
        <a class="entry-title" href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'Nooch' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
        <br>
        <?php if ( 'post' == get_post_type() ) : ?>
        <?php endif; ?>
    </header><!-- .entry-header -->
    <br>
    <?php if ( is_search() ) : // Only display Excerpts for Search ?>
    <div class="featured-image">
    <?php the_post_thumbnail(); ?>
    </div>
    <div class="entry-meta">
            <?php Nooch_posted_on(); ?>
        </div><!-- .entry-meta -->
    <div class="entry-content">
        <?php the_content( __( '<br>Continue reading <span class="meta-nav"-->→', 'Nooch' ) ); ?>
        <?php wp_link_pages( array( 'before' == '<div class="page-links">' . __( 'Pages:', 'Nooch' ), 'after' => '</div>' ) ); ?>
    </div><!-- .entry-content -->
    <img class="line-divider" src="http://nooch.com/00Images/thinhorzbar.png" alt="divider" />
    <?php else : ?>
    <div class="featured-image">
    <?php the_post_thumbnail(); ?>
    </div>
    <div class="entry-meta">
    <small>Posted on <?php the_time('l, F jS, Y') ?> by <?php the_author_meta() ?>.</small>
    </div><!-- .entry-meta -->
    <div class="entry-content">
        <?php the_content( __( '<br>Continue reading <span class="meta-nav"-->→', 'Nooch' ) ); ?>
        <?php wp_link_pages( array( 'before' == '<div class="page-links">' . __( 'Pages:', 'Nooch' ), 'after' => '</div>' ) ); ?>
    </div><!-- .entry-content -->
    <img class="line-divider" src="http://nooch.com/00Images/thinhorzbar.png" alt="divider" />
    <?php endif; ?>
 
    <footer class="entry-comments">
        <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
            <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list( __( ', ', 'Nooch' ) );
                if ( $categories_list && Nooch_categorized_blog() ) :
            ?>
            <span class="cat-links" style="display: none;">
                <?php printf( __( 'Posted in %1$s', 'Nooch' ), $categories_list ); ?>
            </span>
            <?php endif; // End if categories ?>
 
            <?php
                /* translators: used between list items, there is a space after the comma */
                $tags_list = get_the_tag_list( '', __( ', ', 'Nooch' ) );
                if ( $tags_list ) :
            ?>
            <span class="sep" style="display: none;"> | </span>
            <span class="tag-links" style="display: none;">
                <?php printf( __( 'Tagged %1$s', 'Nooch' ), $tags_list ); ?>
            </span>
            <?php endif; // End if $tags_list ?>
        <?php endif; // End if 'post' == get_post_type() ?>
 
        <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
        <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'Nooch' ), __( '1 Comment', 'Nooch' ), __( '% Comments', 'Nooch' ) ); ?></span>
        <?php endif; ?>
        <br>
    </footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->