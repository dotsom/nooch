<?php
/*
Template Name: Request an Invite
*/
?>

<?php get_header(); ?>
<div class="request-wrap page-baseline">
	<div class="head-wrap help-page">
		<h1 class="section-header contact-header mobile-only">Request An Invite</h1>
		<h1 class="section-header contact-header tab-desk-only">Request An Invite</h1>
	</div>
	<div id='request-content-wrapper'>
		<p class="body">Be the first to know when Nooch is available in your area. Jot down a couple of things for us here, and we’ll let you know the <strong><em>instant</em></strong> Nooch is ready for you.</p>

        <form action="http://nooch.us4.list-manage.com/subscribe/post?u=7f2f23cb3666b5cf4efafcef8&amp;id=67adecaac4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <h1 class="reqheader">Hey Nooch!</h1>
            <div class="mc-field-group">
                <p class="reqbody">My <span class="bluetext">name</span> is
                	<input style="text-align: center;" type="text" value="First Lastname*" name="NAME" class="required" id="mce-NAME" onfocus="if(this.value == 'First Lastname*') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'First Lastname*'; }" />
                	and my
                	<span class="bluetext">primary email address</span>
                	is
                	<span class="mmc-field-group">
                		<input type="email" value="you@domain.com*" name="EMAIL" class="required email" id="mce-EMAIL" onfocus="if(this.value == 'you@domain.com*') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'you@domain.com*'; }"/>
                	</span>.  I’d like to be the first to know when Nooch is released, so drop me a line when it’s ready!
                </p>
            </div>
            <input id="request-submit" type="submit">                    
        </form>
    </div>
</div>
<?php get_footer(); ?>