/*
Nooch Scripts File
Author: Tom Peduto
*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36976317-1']);
  _gaq.push(['_setDomainName', 'nooch.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

// as the page loads, call these scripts
jQuery(document).ready(function($) {

//FAQ
    $('#faq-article > ul').addClass('ul-level-1');
    $('.ul-level-1 > li').addClass('li-level-1');
    $('.li-level-1 > ul').addClass('ul-level-2').addClass('collapsed');
    $('.ul-level-2 > li').addClass('li-level-2');
    $('.li-level-2 > ul').addClass('ul-level-3').addClass('collapsed');
    $('.ul-level-3 > li').addClass('li-level-3');

    function allCollapsed(){
        var allClosed = true;
        $('.ul-level-2').each(function(){
            if($(this).hasClass('expanded')){ 
                allClosed = false; 
            }
        });
        $('.ul-level-3').each(function(){
            if($(this).hasClass('expanded')){ 
                allClosed = false; 
            }
        });
        return allClosed;
    }

    function allExpanded(){
        var allOpened = true;
        $('.ul-level-2').each(function(){
            if($(this).hasClass('collapsed')){ 
                allOpened = false; 
            }
        });
        $('.ul-level-3').each(function(){
            if($(this).hasClass('collapsed')){ 
                allOpened = false; 
            }
        });
        return allOpened;
    }
    
    $(".li-level-1, .li-level-2").click(function(event){
        event.stopPropagation();
        $(this).children("ul").slideToggle().toggleClass('collapsed').toggleClass('expanded');
        $(this).children("h2, h3").toggleClass('selected');


        if(!allCollapsed()){
            $('#collapse-all').addClass('actionable');
        } else {
            $('#collapse-all').removeClass('actionable');
        }

        if(!allExpanded()){
            $('#expand-all').addClass('actionable');
        } else {
            $('#expand-all').removeClass('actionable');
        }

        return false;
    });

    $('#collapse-all').click(function(){
        if ($(this).hasClass('actionable')){
            $('#collapse-all').removeClass('actionable');
        }
        if (!$('#expand-all').hasClass('actionable')){
            $('#expand-all').addClass('actionable');
        }
        $('.expanded').parent().children("h2, h3").toggleClass('selected');
        $('.expanded').slideToggle(function(){
            $(this).toggleClass('collapsed').toggleClass('expanded');
        });

        return false;
    });

    $('#expand-all').click(function(){
        if ($(this).hasClass('actionable')){
            $('#expand-all').removeClass('actionable');
        }
        if (!$('#collapse-all').hasClass('actionable')){
            $('#collapse-all').addClass('actionable');
        }
        $('.collapsed').parent().children("h2, h3").toggleClass('selected')
        $('.collapsed').slideToggle(function(){
           $(this).toggleClass('collapsed').toggleClass('expanded');
        });

        return false;
    });

//End FAQ

    $(function(){
        $('#slider-id').liquidSlider({
            autoSlide: true,
            continuous: true,
            slideEaseFunction: "easeInOutQuart"
        });
    });

    var header_nav = $('#header-nav ul');
    var hamb_top = $('div#hamb-top');
    var hamb_mid = $('div#hamb-mid');
    var hamb_bot = $('div#hamb-bot');
    var responsive_viewport = $(window).width();

    function mobileMenu() {
        $('div#hamb-wrap').on('click', function(){
            if (header_nav.height() == 0) {
                var header_nav_on = false;
            } else {
                var header_nav_on = true;
            } 

            if (header_nav_on) {
                header_nav.animate({height: '0px'}, 700, 'easeOutBounce', function(){
                    $(this).removeClass('ul-border');
                    $('header').delay(500).removeClass('darker-boarder');
                });
                hamb_top.removeClass('blue');
                hamb_mid.removeClass('green');
                hamb_bot.removeClass('purp');
            } else {
                header_nav.addClass('ul-border').animate({height: '190px'}, 700, 'easeOutBounce');
                $('header').addClass('darker-boarder');
                hamb_top.addClass('blue');
                hamb_mid.addClass('green');
                hamb_bot.addClass('purp');
            }
        });
    }

    var twitContents = $('.twitter-widget-0').contents().find('html').html();
    console.log(twitContents);

    /*<ol class="h-feed">
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="344203612429373442">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/344203612429373442" data-datetime="2013-06-10T21:25:10+0000"><time pubdate="" class="dt-updated" datetime="2013-06-10T21:25:10+0000" title="Time posted: 10 Jun 2013, 21:25:10 (UTC)" aria-label="Posted on 10 Jun">10 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Can't win em all... "Has Google Wallet Already Failed?"  <a href="http://t.co/po0QvoW85E" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/lTBhG" class="link customisable" target="_blank" title="http://ow.ly/lTBhG"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/lTBhG</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a>  <a href="https://twitter.com/search?q=%23NFC&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>NFC</b></a> <a href="https://twitter.com/search?q=%23mobilepayments&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>mobilepayments</b></a></p>



      <div class="detail-expander">
  <div class="detail-content" data-expanded-media="
  
  
  &lt;div class=&quot;cards-base cards-summary with-image customisable-border&quot;&gt;
  &lt;div class=&quot;cards-content&quot;&gt;
    &lt;h3&gt;
      &lt;a class=&quot;customisable&quot; target=&quot;_blank&quot; href=&quot;http://t.co/po0QvoW85E&quot;&gt;
        &lt;img class=&quot;summary-thumbnail&quot; alt=&quot;&quot;
             src=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhKaHR0cDovL21hcmtldGluZ2xhbmQuY29tL3dwLWNvbnRlbnQvbWwtbG9hZHMvMjAxMi8wOC9nb29nbGUtd2FsbGV0LVBPUy5qcGcUAhYAEgA&amp;amp;s=v3_ODjnHEkAoZfKTPzJb-PTad4mHnEJJYyeFapalsZQ&quot; data-src-2x=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhKaHR0cDovL21hcmtldGluZ2xhbmQuY29tL3dwLWNvbnRlbnQvbWwtbG9hZHMvMjAxMi8wOC9nb29nbGUtd2FsbGV0LVBPUy5qcGcUBBYAEgA&amp;amp;s=az5CeKyy9M2ms4ms_6RBqjnF8P1EYmiAXM4d7ZMvbv0&quot;
             height=&quot;120&quot; width=&quot;120&quot;&gt;
        Has Google Wallet Already Failed?
      &lt;/a&gt;
    &lt;/h3&gt;

    &lt;div class=&quot;byline&quot;&gt;By &lt;a class=&quot;byline-user profile h-card&quot; href=&quot;https://twitter.com/gsterling&quot;
  &gt;&lt;span class=&quot;p-name&quot;&gt;Greg Sterling&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;gsterling&lt;/b&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;

    &lt;p class=&quot;article&quot;&gt;
      &lt;a target=&quot;_blank&quot; href=&quot;http://t.co/po0QvoW85E&quot;&gt;Businessweek ran an article today in which it assesses the current state of Google’s Wallet efforts. Osama Bedier, who came from PayPal and ran the Google Wallet initiative, is leaving the company....&lt;/a&gt;
    &lt;/p&gt;
  &lt;/div&gt;
  &lt;a class=&quot;source-user profile h-card&quot; href=&quot;https://twitter.com/Marketingland&quot;&gt;
  &lt;img class=&quot;u-photo avatar&quot; alt=&quot;&quot;
       src=&quot;https://si0.twimg.com/profile_images/1641238264/MLand-Logo-Veritcal-250x250_mini.png&quot; data-src-2x=&quot;https://si0.twimg.com/profile_images/1641238264/MLand-Logo-Veritcal-250x250_normal.png&quot;&gt;
  &lt;span class=&quot;p-name&quot;&gt;Marketing Land&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;Marketingland&lt;/b&gt;&lt;/span&gt;
&lt;/a&gt;
&lt;/div&gt;
"></div>
</div>
  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-favorites">
    <strong>1</strong> favorite
  </span>
</span>
</span>
    
  <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/344203612429373442" data-toggled-text="Hide Summary"><i class="ic-sum ic-mask"></i><b>Show Summary</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=344203612429373442" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=344203612429373442" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=344203612429373442" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-favorites">
    <strong>1</strong> favorite
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="343443756395147264">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/343443756395147264" data-datetime="2013-06-08T19:05:47+0000"><time pubdate="" class="dt-updated" datetime="2013-06-08T19:05:47+0000" title="Time posted: 08 Jun 2013, 19:05:47 (UTC)" aria-label="Posted on 08 Jun">8 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">"And most important, have the courage to follow your heart &amp; intuition. They somehow alrdy know what you truly want to become.” -Steve Jobs</p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=343443756395147264" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=343443756395147264" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=343443756395147264" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="343076753159356416">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/343076753159356416" data-datetime="2013-06-07T18:47:26+0000"><time pubdate="" class="dt-updated" datetime="2013-06-07T18:47:26+0000" title="Time posted: 07 Jun 2013, 18:47:26 (UTC)" aria-label="Posted on 07 Jun">7 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Gr8 reminder: “It takes 20 years to build a reputation &amp; 5 mins to ruin it. If you think about that, you’ll do things differently.” -Buffett</p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=343076753159356416" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=343076753159356416" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=343076753159356416" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="342281321885859840">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/342281321885859840" data-datetime="2013-06-05T14:06:41+0000"><time pubdate="" class="dt-updated" datetime="2013-06-05T14:06:41+0000" title="Time posted: 05 Jun 2013, 14:06:41 (UTC)" aria-label="Posted on 05 Jun">5 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Indeed, sir:  “I have not failed. I’ve just found 10,000 ways that won’t work.” |  Thomas Edison  <a href="https://twitter.com/search?q=%23MobilePayments&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>MobilePayments</b></a> <a href="https://twitter.com/search?q=%23startup&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>startup</b></a> <a href="https://twitter.com/search?q=%23innovation&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>innovation</b></a> <a href="https://twitter.com/search?q=%23tech&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>tech</b></a></p>



  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
    
    <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/342281321885859840" data-toggled-text="Collapse"><b>Expand</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=342281321885859840" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=342281321885859840" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=342281321885859840" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="341928801728610305">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/341928801728610305" data-datetime="2013-06-04T14:45:53+0000"><time pubdate="" class="dt-updated" datetime="2013-06-04T14:45:53+0000" title="Time posted: 04 Jun 2013, 14:45:53 (UTC)" aria-label="Posted on 04 Jun">4 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Thoughtful <a href="https://twitter.com/search?q=%23TED&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>TED</b></a> from <a href="https://twitter.com/danpallotta" class="profile customisable h-card" dir="ltr">@<b class="p-nickname">danpallotta</b></a>: "Our generation doesnt want its epitaph to read: 'We kept charity overhead low'"  <a href="http://t.co/bVWgg1lS40" rel="nofollow" dir="ltr" data-expanded-url="http://on.ted.com/bbSV" class="link customisable" target="_blank" title="http://on.ted.com/bbSV"><span class="tco-hidden">http://</span><span class="tco-display">on.ted.com/bbSV</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



      <div class="detail-expander">
  <div class="detail-content" data-expanded-media="
  
  
  &lt;div class=&quot;cards-base cards-summary with-image customisable-border&quot;&gt;
  &lt;div class=&quot;cards-content&quot;&gt;
    &lt;h3&gt;
      &lt;a class=&quot;customisable&quot; target=&quot;_blank&quot; href=&quot;http://t.co/bVWgg1lS40&quot;&gt;
        &lt;img class=&quot;summary-thumbnail&quot; alt=&quot;&quot;
             src=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhVaHR0cDovL2ltYWdlcy50ZWQuY29tL2ltYWdlcy90ZWQvZDU1NzdmZGZhNjUyNGYwYjkxYTAwZmQ4ZDlkZjg0ODEwZmI1YTEwY18zODl4MjkyLmpwZxQCFgASAA&amp;amp;s=KADC-Y6PU0TSBye_PfQJ9gwkDtGqQ0mqwrbP9acHu_M&quot; data-src-2x=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhVaHR0cDovL2ltYWdlcy50ZWQuY29tL2ltYWdlcy90ZWQvZDU1NzdmZGZhNjUyNGYwYjkxYTAwZmQ4ZDlkZjg0ODEwZmI1YTEwY18zODl4MjkyLmpwZxQEFgASAA&amp;amp;s=uiRndOz1yIKrStsbDETONaX-s-Y6SGO-uBt16XvGTl8&quot;
             height=&quot;120&quot; width=&quot;120&quot;&gt;
        Dan Pallotta: The way we think about charity is dead wrong | Video on...
      &lt;/a&gt;
    &lt;/h3&gt;


    &lt;p class=&quot;article&quot;&gt;
      &lt;a target=&quot;_blank&quot; href=&quot;http://t.co/bVWgg1lS40&quot;&gt;Activist and fundraiser Dan Pallotta calls out the double standard that drives our broken relationship to charities. Too many nonprofits, he says, are rewarded for how little they spend -- not for...&lt;/a&gt;
    &lt;/p&gt;
  &lt;/div&gt;
  &lt;a class=&quot;source-user profile h-card&quot; href=&quot;https://twitter.com/tedtalks&quot;&gt;
  &lt;img class=&quot;u-photo avatar&quot; alt=&quot;&quot;
       src=&quot;https://si0.twimg.com/profile_images/3278476079/cb1f8cb5c856e6b5dfb9f79558b5728c_mini.png&quot; data-src-2x=&quot;https://si0.twimg.com/profile_images/3278476079/cb1f8cb5c856e6b5dfb9f79558b5728c_normal.png&quot;&gt;
  &lt;span class=&quot;p-name&quot;&gt;TEDTalks Updates&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;tedtalks&lt;/b&gt;&lt;/span&gt;
&lt;/a&gt;
&lt;/div&gt;
"></div>
</div>
  </div>

  <div class="footer customisable-border">
    
    
  <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/341928801728610305" data-toggled-text="Hide Summary"><i class="ic-sum ic-mask"></i><b>Show Summary</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=341928801728610305" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=341928801728610305" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=341928801728610305" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="341542577041973250">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/341542577041973250" data-datetime="2013-06-03T13:11:10+0000"><time pubdate="" class="dt-updated" datetime="2013-06-03T13:11:10+0000" title="Time posted: 03 Jun 2013, 13:11:10 (UTC)" aria-label="Posted on 03 Jun">3 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">One of our favorites: “Winners never quit and quitters never win.”  - Vince Lombardi  <a href="https://twitter.com/search?q=%23startup&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>startup</b></a> <a href="https://twitter.com/search?q=%23innovation&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>innovation</b></a> <a href="https://twitter.com/search?q=%23perseverance&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>perseverance</b></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=341542577041973250" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=341542577041973250" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=341542577041973250" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="341269536026275840">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/341269536026275840" data-datetime="2013-06-02T19:06:12+0000"><time pubdate="" class="dt-updated" datetime="2013-06-02T19:06:12+0000" title="Time posted: 02 Jun 2013, 19:06:12 (UTC)" aria-label="Posted on 02 Jun">2 Jun</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">“Entrepreneurship is living a few years of your life like most people won’t so you can spend the rest of your life like most people cant.”</p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=341269536026275840" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=341269536026275840" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=341269536026275840" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="340511865887158272">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340511865887158272" data-datetime="2013-05-31T16:55:29+0000"><time pubdate="" class="dt-updated" datetime="2013-05-31T16:55:29+0000" title="Time posted: 31 May 2013, 16:55:29 (UTC)" aria-label="Posted on 31 May">31 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Why Mobile Payments Are Set To Explode - <a href="https://twitter.com/SAI" class="profile customisable h-card" dir="ltr">@<b class="p-nickname">sai</b></a>  <a href="http://t.co/Tl17nqf50F" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/lApeL" class="link customisable" target="_blank" title="http://ow.ly/lApeL"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/lApeL</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



      <div class="detail-expander">
  <div class="detail-content" data-expanded-media="
  
  
  &lt;div class=&quot;cards-base cards-summary with-image customisable-border&quot;&gt;
  &lt;div class=&quot;cards-content&quot;&gt;
    &lt;h3&gt;
      &lt;a class=&quot;customisable&quot; target=&quot;_blank&quot; href=&quot;http://t.co/Tl17nqf50F&quot;&gt;
        &lt;img class=&quot;summary-thumbnail&quot; alt=&quot;&quot;
             src=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhsaHR0cDovL3N0YXRpYzUuYnVzaW5lc3NpbnNpZGVyLmNvbS9pbWFnZS81MWE3N2ExYjZiYjNmNzFjNjgwMDAwMDIvd2h5LW1vYmlsZS1wYXltZW50cy1hcmUtc2V0LXRvLWV4cGxvZGUuanBnFAIWABIA&amp;amp;s=9fE6lUaFDlv9m9IpaNjgNBmGXCVcPY63rsYPfYYEQ34&quot; data-src-2x=&quot;https://o.twimg.com/1/proxy.jpg?t=FQIVBhhsaHR0cDovL3N0YXRpYzUuYnVzaW5lc3NpbnNpZGVyLmNvbS9pbWFnZS81MWE3N2ExYjZiYjNmNzFjNjgwMDAwMDIvd2h5LW1vYmlsZS1wYXltZW50cy1hcmUtc2V0LXRvLWV4cGxvZGUuanBnFAQWABIA&amp;amp;s=iRvGBEusmKwaLm4TnByYsAqBOxGIy7C_vrm0nJbXKDw&quot;
             height=&quot;120&quot; width=&quot;120&quot;&gt;
        Why Mobile Payments Are Set To Explode
      &lt;/a&gt;
    &lt;/h3&gt;

    &lt;div class=&quot;byline&quot;&gt;By &lt;a class=&quot;byline-user profile h-card&quot; href=&quot;https://twitter.com/joshluger&quot;
  &gt;&lt;span class=&quot;p-name&quot;&gt;Josh Luger&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;joshluger&lt;/b&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;

    &lt;p class=&quot;article&quot;&gt;
      &lt;a target=&quot;_blank&quot; href=&quot;http://t.co/Tl17nqf50F&quot;&gt;With more than $3.95 trillion of non-cash transaction volume recorded in the U.S. in 2011, the stakes are high.&lt;/a&gt;
    &lt;/p&gt;
  &lt;/div&gt;
  &lt;a class=&quot;source-user profile h-card&quot; href=&quot;https://twitter.com/SAI&quot;&gt;
  &lt;img class=&quot;u-photo avatar&quot; alt=&quot;&quot;
       src=&quot;https://si0.twimg.com/profile_images/1227602539/bi-sai_mini.png&quot; data-src-2x=&quot;https://si0.twimg.com/profile_images/1227602539/bi-sai_normal.png&quot;&gt;
  &lt;span class=&quot;p-name&quot;&gt;SAI&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;SAI&lt;/b&gt;&lt;/span&gt;
&lt;/a&gt;
&lt;/div&gt;
"></div>
</div>
  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
    
  <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340511865887158272" data-toggled-text="Hide Summary"><i class="ic-sum ic-mask"></i><b>Show Summary</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=340511865887158272" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=340511865887158272" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=340511865887158272" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="340502378077900800">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340502378077900800" data-datetime="2013-05-31T16:17:47+0000"><time pubdate="" class="dt-updated" datetime="2013-05-31T16:17:47+0000" title="Time posted: 31 May 2013, 16:17:47 (UTC)" aria-label="Posted on 31 May">31 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Cash not done yet... Head of ATM trade group on ATMs, Mobile &amp; Money: the future of accessing funds | <a href="https://twitter.com/MobilePayToday" class="profile customisable h-card" dir="ltr">@<b class="p-nickname">MobilePayToday</b></a> <a href="http://t.co/FRv2vhOE5C" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/lAowO" class="link customisable" target="_blank" title="http://ow.ly/lAowO"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/lAowO</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
    
    <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340502378077900800" data-toggled-text="Collapse"><b>Expand</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=340502378077900800" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=340502378077900800" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=340502378077900800" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-retweets">
    <strong>1</strong> Retweet
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="340456653575049217">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340456653575049217" data-datetime="2013-05-31T13:16:06+0000"><time pubdate="" class="dt-updated" datetime="2013-05-31T13:16:06+0000" title="Time posted: 31 May 2013, 13:16:06 (UTC)" aria-label="Posted on 31 May">31 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Wise ol' Dr. Huxtable: “I don’t know the key to success, but the key to failure is trying to please everybody.” - Bill Cosby  <a href="https://twitter.com/search?q=%23startup&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>startup</b></a></p>



  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-retweets">
    <strong>3</strong> Retweets
  </span>
</span>
</span>
    
    <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/340456653575049217" data-toggled-text="Collapse"><b>Expand</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=340456653575049217" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=340456653575049217" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=340456653575049217" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-retweets">
    <strong>3</strong> Retweets
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="339795265710084096">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/339795265710084096" data-datetime="2013-05-29T17:27:59+0000"><time pubdate="" class="dt-updated" datetime="2013-05-29T17:27:59+0000" title="Time posted: 29 May 2013, 17:27:59 (UTC)" aria-label="Posted on 29 May">29 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">RT <a href="https://twitter.com/TechnicallyPHL" class="profile customisable h-card" dir="ltr">@<b class="p-nickname">technicallyphl</b></a>: Joint Ventures 101: Random Hacks of Kindness Reception <a href="http://t.co/cAKGcuJDj6" rel="nofollow" dir="ltr" data-expanded-url="http://ph.ly/P8QEG" class="link customisable" target="_blank" title="http://ph.ly/P8QEG"><span class="tco-hidden">http://</span><span class="tco-display">ph.ly/P8QEG</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=339795265710084096" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=339795265710084096" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=339795265710084096" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="339715505248149504">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/339715505248149504" data-datetime="2013-05-29T12:11:02+0000"><time pubdate="" class="dt-updated" datetime="2013-05-29T12:11:02+0000" title="Time posted: 29 May 2013, 12:11:02 (UTC)" aria-label="Posted on 29 May">29 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title"><a href="https://twitter.com/search?q=%23Truism&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>Truism</b></a>  “The best way to predict the future is to create it.”  - Peter Drucker  <a href="https://twitter.com/search?q=%23startup&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>startup</b></a> <a href="https://twitter.com/search?q=%23innovation&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>innovation</b></a> <a href="https://twitter.com/search?q=%23tech&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>tech</b></a></p>



  </div>

  <div class="footer customisable-border">
    <span class="stats-narrow"><span class="stats">
  <span class="stats-retweets">
    <strong>2</strong> Retweets
  </span>
</span>
</span>
    
    <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/339715505248149504" data-toggled-text="Collapse"><b>Expand</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=339715505248149504" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=339715505248149504" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=339715505248149504" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    <span class="stats-wide"><b>· </b><span class="stats">
  <span class="stats-retweets">
    <strong>2</strong> Retweets
  </span>
</span>
</span>
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="339696439330496512">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/339696439330496512" data-datetime="2013-05-29T10:55:17+0000"><time pubdate="" class="dt-updated" datetime="2013-05-29T10:55:17+0000" title="Time posted: 29 May 2013, 10:55:17 (UTC)" aria-label="Posted on 29 May">29 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Not sure that POS is right beachhead, but banks are the key in the US: Banks Must Get Into the Mobile POS Game Now | <a href="http://t.co/TwUPcgVrCL" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/luoQR" class="link customisable" target="_blank" title="http://ow.ly/luoQR"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/luoQR</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=339696439330496512" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=339696439330496512" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=339696439330496512" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="339140061562023938">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/339140061562023938" data-datetime="2013-05-27T22:04:26+0000"><time pubdate="" class="dt-updated" datetime="2013-05-27T22:04:26+0000" title="Time posted: 27 May 2013, 22:04:26 (UTC)" aria-label="Posted on 27 May">27 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">“If you cannot do great things, do small things in a great way.” -Napoleon Hill. Here's to the many great men &amp; women who have kept us free.</p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=339140061562023938" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=339140061562023938" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=339140061562023938" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="335137269193273344">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/335137269193273344" data-datetime="2013-05-16T20:58:46+0000"><time pubdate="" class="dt-updated" datetime="2013-05-16T20:58:46+0000" title="Time posted: 16 May 2013, 20:58:46 (UTC)" aria-label="Posted on 16 May">16 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">The FTC released a report and recommendations for companies facilitating <a href="https://twitter.com/search?q=%23mpayments&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>mpayments</b></a> | <a href="http://t.co/MFFr8y3axT" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/l6REU" class="link customisable" target="_blank" title="http://ow.ly/l6REU"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/l6REU</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=335137269193273344" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=335137269193273344" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=335137269193273344" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="334740771435593728">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/334740771435593728" data-datetime="2013-05-15T18:43:13+0000"><time pubdate="" class="dt-updated" datetime="2013-05-15T18:43:13+0000" title="Time posted: 15 May 2013, 18:43:13 (UTC)" aria-label="Posted on 15 May">15 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">3 myths about Apple and <a href="https://twitter.com/search?q=%23mpayments&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>mpayments</b></a> | <a href="http://t.co/LCm6EC0USq" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/l46n3" class="link customisable" target="_blank" title="http://ow.ly/l46n3"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/l46n3</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a> | Mobile payments don't need Apple, it's the other way around.</p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=334740771435593728" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=334740771435593728" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=334740771435593728" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="334707255368900608">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/334707255368900608" data-datetime="2013-05-15T16:30:02+0000"><time pubdate="" class="dt-updated" datetime="2013-05-15T16:30:02+0000" title="Time posted: 15 May 2013, 16:30:02 (UTC)" aria-label="Posted on 15 May">15 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">An article written by the Nooch team was published in Cards &amp; Payments Intelligence! | <a href="http://t.co/37FQEQA7rS" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/l3LKj" class="link customisable" target="_blank" title="http://ow.ly/l3LKj"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/l3LKj</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=334707255368900608" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=334707255368900608" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=334707255368900608" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="333925952956555264">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/333925952956555264" data-datetime="2013-05-13T12:45:25+0000"><time pubdate="" class="dt-updated" datetime="2013-05-13T12:45:25+0000" title="Time posted: 13 May 2013, 12:45:25 (UTC)" aria-label="Posted on 13 May">13 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title"><a href="https://twitter.com/search?q=%23MobileBanking&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>MobileBanking</b></a> can be safe &amp; secure, here are a few common sense tips from the FDIC to protect yourself: <a href="http://t.co/LNInfTwcrP" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/kXmLP" class="link customisable" target="_blank" title="http://ow.ly/kXmLP"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/kXmLP</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=333925952956555264" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=333925952956555264" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=333925952956555264" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry with-expansion customisable-border" data-tweet-id="333661644989267968">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/333661644989267968" data-datetime="2013-05-12T19:15:09+0000"><time pubdate="" class="dt-updated" datetime="2013-05-12T19:15:09+0000" title="Time posted: 12 May 2013, 19:15:09 (UTC)" aria-label="Posted on 12 May">12 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">Payday loans are "Like a drug"...  Payday loan users hooked on quick-cash cycle" <a href="http://t.co/AYP6ClPco6" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/kWWwY" class="link customisable" target="_blank" title="http://ow.ly/kWWwY"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/kWWwY</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a></p>



      <div class="detail-expander">
  <div class="detail-content" data-expanded-media="
  
  
  &lt;div class=&quot;cards-base cards-summary  customisable-border&quot;&gt;
  &lt;div class=&quot;cards-content&quot;&gt;
    &lt;h3&gt;
      &lt;a class=&quot;customisable&quot; target=&quot;_blank&quot; href=&quot;http://t.co/AYP6ClPco6&quot;&gt;
        &amp;#39;Like a drug&amp;#39;: Payday loan users hooked on quick-cash cycle
      &lt;/a&gt;
    &lt;/h3&gt;


    &lt;p class=&quot;article&quot;&gt;
      &lt;a target=&quot;_blank&quot; href=&quot;http://t.co/AYP6ClPco6&quot;&gt;By Bob Sullivan, Senior Writer, NBC News For Raymond Chaney, taking out a&nbsp;payday loan was like hiring a taxi to drive across the country. He ended up broke —&nbsp;and stranded. The 66-year-old veteran...&lt;/a&gt;
    &lt;/p&gt;
  &lt;/div&gt;
  &lt;a class=&quot;source-user profile h-card&quot; href=&quot;https://twitter.com/NBCNews&quot;&gt;
  &lt;img class=&quot;u-photo avatar&quot; alt=&quot;&quot;
       src=&quot;https://si0.twimg.com/profile_images/3589008446/5e3a7cedd593ac9e7d9fb70dadb1ef8f_mini.jpeg&quot; data-src-2x=&quot;https://si0.twimg.com/profile_images/3589008446/5e3a7cedd593ac9e7d9fb70dadb1ef8f_normal.jpeg&quot;&gt;
  &lt;span class=&quot;p-name&quot;&gt;NBC News&lt;/span&gt;
  &lt;span class=&quot;p-nickname&quot; dir=&quot;ltr&quot;&gt;@&lt;b&gt;NBCNews&lt;/b&gt;&lt;/span&gt;
&lt;/a&gt;
&lt;/div&gt;
"></div>
</div>
  </div>

  <div class="footer customisable-border">
    
    
  <a class="expand customisable-highlight" href="https://twitter.com/NoochMoney/statuses/333661644989267968" data-toggled-text="Hide Summary"><i class="ic-sum ic-mask"></i><b>Show Summary</b></a>

    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=333661644989267968" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=333661644989267968" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=333661644989267968" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
      <li class="tweet h-entry customisable-border" data-tweet-id="333299239230767104">
  








<a class="u-url permalink customisable-highlight" href="https://twitter.com/NoochMoney/statuses/333299239230767104" data-datetime="2013-05-11T19:15:05+0000"><time pubdate="" class="dt-updated" datetime="2013-05-11T19:15:05+0000" title="Time posted: 11 May 2013, 19:15:05 (UTC)" aria-label="Posted on 11 May">11 May</time></a>

  <div class="header h-card p-author">
  <a class="u-url profile" href="https://twitter.com/NoochMoney" aria-label="Nooch (screen name: NoochMoney)">
    <img class="u-photo avatar" alt="" src="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_normal.png" data-src-2x="https://si0.twimg.com/profile_images/2957821643/29e3042294601a7f5903d1f197d0ec68_bigger.png" width="48" height="48">
    <span class="full-name">
      
      <span class="p-name customisable-highlight">Nooch</span>
    </span>
    <span class="p-nickname" dir="ltr">@<b>NoochMoney</b></span>
  </a>
</div>

  <div class="e-entry-content">
    <p class="e-entry-title">You sir, are correct.  "Startup names don’t have to be terrible. Here’s the proof. <a href="http://t.co/vEzhrUPUa5" rel="nofollow" dir="ltr" data-expanded-url="http://ow.ly/kW49v" class="link customisable" target="_blank" title="http://ow.ly/kW49v"><span class="tco-hidden">http://</span><span class="tco-display">ow.ly/kW49v</span><span class="tco-hidden"></span><span class="tco-ellipsis"><span class="tco-hidden">&nbsp;</span></span></a>  <a href="https://twitter.com/VentureBeat" class="profile customisable h-card" dir="ltr">@<b class="p-nickname">VentureBeat</b></a>  <a href="https://twitter.com/search?q=%23NobodyLikesaMooch&amp;src=hash" data-query-source="hashtag_click" class="hashtag customisable" dir="ltr" rel="tag">#<b>NobodyLikesaMooch</b></a></p>



  </div>

  <div class="footer customisable-border">
    
    
    <ul class="tweet-actions">
  <li><a href="https://twitter.com/intent/tweet?in_reply_to=333299239230767104" class="reply-action web-intent" title="Reply"><i class="ic-reply ic-mask"></i><b>Reply</b></a></li>
  <li><a href="https://twitter.com/intent/retweet?tweet_id=333299239230767104" class="retweet-action web-intent" title="Retweet"><i class="ic-retweet ic-mask"></i><b>Retweet</b></a></li>
  <li><a href="https://twitter.com/intent/favorite?tweet_id=333299239230767104" class="favorite-action web-intent" title="Favorite"><i class="ic-fav ic-mask"></i><b>Favorite</b></a></li>
</ul>
    
  </div>
</li>
    </ol>*/
        
    mobileMenu();
        
    if (responsive_viewport < 768) {
        var alreadyLoaded = true;
    }
    if (responsive_viewport > 481) {        
    }
    if (responsive_viewport >= 768) {
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });
    }
    if (responsive_viewport > 1030) {
    }

    /*Catches window resize to mobile events
    $(window).resize(function () {
        responsive_viewport = $(window).width();
        if (responsive_viewport < 768 && !alreadyLoaded) {
            mobileMenu();
        }
    });*/
});

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );