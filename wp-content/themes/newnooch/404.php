<?php get_header(); ?>
<div class="head-wrap about-page">
	<h1 class="section-header error-header mobile-only">Something seems... off</h1>
</div>
			<div id="content" class="killbot">
				<div id="inner-content" class="wrap clearfix">
					<div id="main" class="eightcol first clearfix main-404" role="main">
    					<img alt="Big Killer Robot!" class="killbot-mobile" src="<?php echo get_template_directory_uri(); ?>/library/images/killerbot.png" />
    					
    					<h1 class="tab-desk-only">Something seems fishy around here...</h1>

    					<p>It appears that you've reached this page in error.  That may have something to do with this rampaging ATM over here, it may be that you've mistyped a URL, or it could just be that the page that you're looking for no longer exists.  Whatever the case, you should hurry on out of here... we'll try to keep the beast at bay!<br><br><strong>Fly, you fools!</strong></p>		

					</div> <!-- end #main -->
				</div> <!-- end #inner-content -->
			</div> <!-- end #content -->
<?php get_footer(); ?>
