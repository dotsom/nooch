<?php get_header(); ?>

				<!--Mobile Main-->

				<div id="home-content-top" class="content-container mobile-only">

					<h1>The quickest way to send money is right in your pocket</h1>

					<div id="main-body-image"></div>

					<a href="<?php echo get_page_link(1830); ?>" id="main-body-button" role="button">Request an Invite!</a>

					<p>Nooch is the simplest way to send money to anyone, anywhere, anytime. As long as you know a name, you know enough.</p>

					<a href="<?php echo get_page_link(1823); ?>" id="main-body-link">Learn more ></a>

				</div>



				<!--Comp/tab Main-->

			<div class="full-width tab-desk-only">

				<div class="liquid-slider"  id="slider-id">



						<?php

						$slider_args = array( "post_type" => "home_page", "category_name" => "main-slides");

						$slider_query = new WP_Query( $slider_args );

						$slider_count = 1;

						while ( $slider_query->have_posts() ) : $slider_query->the_post();



						$slider_id = $post->ID;

						$slider_text = get_post_meta( $slider_id, "slider_link_text", true);

						$slider_link = get_post_meta( $slider_id, "slider_link", true);

						$slider_color = get_post_meta( $slider_id, "slider_link_color", true);



						if ( $slider_count == 1 ) { ?>

						<div id="slide<?php echo $slider_count; ?>" class="slide">

        					<div id="playcontain">

        						<a id="playbutton" rel="shadowbox;height=360;width=630" href="https://www.youtube.com/embed/DW80JinG2zU?autoplay=1&rel=0&width=640&height=360">watch a video</a>

        					</div>

        					<div id="textcontain">

          						<h2><?php the_title(); ?></h2>

          						<?php the_content(); ?>

          						<a href="<?php echo $slider_link; ?>" class="button <?php echo $slider_color; ?>-but"><?php echo $slider_text; ?></a>

          					</div>							

          				</div>

						<?php } ?>



						<?php if ( $slider_count > 1) { ?>

						

						<div id="slide<?php echo $slider_count; ?>" class="slide">

          					<h2><?php the_title(); ?></h2>

          					<?php the_content(); ?>

          					<a href="<?php echo $slider_link; ?>" class="button <?php echo $slider_color; ?>-but"><?php echo $slider_text; ?></a>

        				</div>



        				<?php } ?>



        				<?php $slider_count++; ?>

						<?php endwhile; ?>



      			</div>

      		</div>





				<div id="section-divider"></div>

				<div id="home-content-mid" class="content-container">

					<ul>



						<?php

							$home_main_args = array("post_type" => "home_page", "posts_per_page" => 3, "category_name" => "home-main");

							$home_main_query = new WP_Query( $home_main_args );



							while ( $home_main_query->have_posts() ) : $home_main_query->the_post(); 

								$post_id = get_the_ID();

								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	

						?>

						

						<li class="bucket-wrap" style="background-image: url('<?php echo $image[0]; ?>')">

							<h1><?php the_title(); ?></h1>

							<?php the_content(); ?>

						</li>

						<?php endwhile; ?>



					</ul>

				</div>

				<div id="section-divider"></div>

				<div id="quickeasycontainer" class="tab-desk-only section-wrapper">

    				

						<?php

							$quick_header_args = array( "post_type" => "home_page", "category_name" => "heading-main-quick");

							$quick_header_query = new WP_Query( $quick_header_args );



							while ( $quick_header_query->have_posts() ) : $quick_header_query->the_post();

						?>

						<h1 class="home-section-header"><?php the_title(); ?></h1>

				    	<?php the_content(); ?>

						<?php endwhile; ?>



				    	<ul>



						<?php

							$quick_item_args = array("post_type" => "home_page", "posts_per_page" => 3, "category_name" => "item-main-quick");

							$quick_item_query = new WP_Query( $quick_item_args );

							$quick_count = 1;



							while ( $quick_item_query->have_posts() ) : $quick_item_query->the_post(); 

								$post_id = get_the_ID();

								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	

						?>

						

						<li class="quick-bucket-wrap" id="quick-bucket-<?php echo $quick_count; ?>" style="background-image: url('<?php echo $image[0]; ?>')">

							<h4><?php the_title(); ?></h4>

							<?php the_content(); ?>

						</li>

						<?php $quick_count++ ?>

						<?php endwhile; ?>

						</ul>



				</div>

				<div id="anytimecontainer" class="tab-desk-only section-wrapper">

    				

						<?php

							$any_header_args = array( "post_type" => "home_page", "category_name" => "heading-main-anytime");

							$any_header_query = new WP_Query( $any_header_args );



							while ( $any_header_query->have_posts() ) : $any_header_query->the_post();

						?>

						<h1 class="home-section-header"><?php the_title(); ?></h1>

				    	<?php the_content(); ?>

						<?php endwhile; ?>



				    <ul>



						<?php

							$any_item_args = array("post_type" => "home_page", "posts_per_page" => 3, "category_name" => "item-main-anytime");

							$any_item_query = new WP_Query( $any_item_args );

							$any_count = 1;



							while ( $any_item_query->have_posts() ) : $any_item_query->the_post(); 

								$post_id = get_the_ID();

								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	

						?>

						

						<li class="any-bucket-wrap" id="any-bucket-<?php echo $any_count; ?>" style="background-image: url('<?php echo $image[0]; ?>')">

							<h4><?php the_title(); ?></h4>

							<?php the_content(); ?>

						</li>

						<?php $any_count++ ?>

						<?php endwhile; ?>

					</ul>

				</div>



				<div id="safesecurecontainer" class="tab-desk-only section-wrapper">

    				

						<?php

							$safe_header_args = array( "post_type" => "home_page", "category_name" => "heading-main-safety");

							$safe_header_query = new WP_Query( $safe_header_args );



							while ( $safe_header_query->have_posts() ) : $safe_header_query->the_post();

						?>

						<h1 class="home-section-header"><?php the_title(); ?></h1>

				    	<?php the_content(); ?>

						<?php endwhile; ?>



				    <ul>



						<?php

							$safe_item_args = array("post_type" => "home_page", "posts_per_page" => 3, "category_name" => "item-main-safety");

							$safe_item_query = new WP_Query( $safe_item_args );

							$safe_count = 1;



							while ( $safe_item_query->have_posts() ) : $safe_item_query->the_post(); 

								$post_id = get_the_ID();

								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );	

						?>

						

						<li class="safe-bucket-wrap" id="safe-bucket-<?php echo $safe_count; ?>" style="background-image: url('<?php echo $image[0]; ?>')">

								<h4><?php the_title(); ?></h4>

							<?php the_content(); ?>

						</li>

						<?php $safe_count++ ?>

						<?php endwhile; ?>

					</ul>

				</div>



				<div id="awards">

    <div id="awards-section">

    	<ul>

    		<li><h2 class="awardstext"> Awards &amp; Recognition</h2></li>

    		<li><a href="https://www.dukestartupchallenge.org/2010-2011/epc"><img id="award1" src="<?php echo get_template_directory_uri(); ?>/library/images/awards1.png"></a></li>

    		<li><a href="https://www.cednc.org/"><img id="award2" src="<?php echo get_template_directory_uri(); ?>/library/images/awards2.png"></a></li>

    		<li><a href="https://www.youtube.com/watch?v=v_1-HW0611E"><img id="award3" src="<?php echo get_template_directory_uri(); ?>/library/images/awards3.png"></a></li>

    		<li><a href="https://www.plugandplaytechcenter.com/"><img id="award4" src="<?php echo get_template_directory_uri(); ?>/library/images/awards4.png"></a></li>

    		<li><a href="https://nooch.com/nooch-to-present-at-mobile-monday-mid-atlantic/"><img id="award5" src="<?php echo get_template_directory_uri(); ?>/library/images/awards5.png"></a></li>

    		<li><a href="https://www2.lebow.drexel.edu/Baiada/Competitions/BusinessPlan/PastWinners.php"><img id="award6" src="<?php echo get_template_directory_uri(); ?>/library/images/awards6.png"></a></li>

    	</ul>

    </div>  

  </div>

				        

<?php get_footer(); ?>

