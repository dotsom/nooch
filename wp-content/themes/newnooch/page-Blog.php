<?php

/*

Template Name: Blog

1819*/

?>



<?php get_header(); ?>

<?php $headquery = new WP_Query( array( 'page_id' => 1819) );

while ( $headquery->have_posts() ) : $headquery->the_post(); ?> 



<div class="outer-wrap">

    <div class="head-wrap blog-page">

        <h1 class="section-header blog-header tab-desk-only"><?php the_title() ?></h1>

    </div>

<div class="blog-wrap page-baseline">

    <div class="head-wrap about-page mobile-only">

        <h1 class="section-header blog-header mobile-only"><?php the_title() ?></h1>

    </div>



<?php endwhile;



$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$noochquery = new WP_Query( array( 'posts_per_page' => 5, 'max_num_pages' => 10, 'paged' => $paged ) );



global $wp_query;

$temp_wp_query = $wp_query;

$wp_query = null;

$wp_query = $noochquery;



while ( $noochquery->have_posts() ) : $noochquery->the_post(); ?> 



    <article class="blog-article" id="post-<?php the_ID(); ?>">



        <header class="entry-header">

            <img alt="<?php the_author_meta("first_name"); ?>" class="tab-desk-only team-profile-pic" src="<?php echo get_template_directory_uri(); ?>/library/images/teampics/<?php the_author_meta("nickname")?>.png" />

            <a class='entry-header-link' href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

        </header>



        <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'mobile-image', NULL, "mobile-image-600"); endif; ?>



        <a class="featured-image"href="<?php echo get_permalink(); ?>">

            <img class="feat-img" width="744" height="300" src="<?php $img=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(750, 304)); echo $img[0]; ?>" alt="<?php the_title(); ?>"/>

        </a>



        <div class='text-wrapper'>



            <?php the_excerpt(); ?>



        </div>

    </article>



<?php endwhile; ?>



</div>

<nav class="bottom-nav-wrap mobile-only">



<div class="nav-numbers">



<?php

if($noochquery->max_num_pages>1){

    for($i=1; $i<=$noochquery->max_num_pages; $i++){?>

        <a href="<?php echo '?paged=' . $i; ?>" <?php echo ($paged==$i)? 'class="selected"':'';?>><?php echo $i;?></a>

        <?php

    }

}

?>



</div>



<div id="previous-wrap">

<?php 

previous_posts_link("<< Prev");

?>

</div><div id="next-wrap">

<?php

next_posts_link("Next >>");

?>

</div>

</nav>



<?php get_sidebar(); ?>

</div>



<nav class="bottom-nav-wrap tab-desk-only">



<div class="nav-numbers">



<?php

if($noochquery->max_num_pages>1){

    for($i=1; $i<=$noochquery->max_num_pages; $i++){?>

        <a href="<?php echo '?paged=' . $i; ?>" <?php echo ($paged==$i)? 'class="selected"':'';?>><?php echo $i;?></a>

        <?php

    }

}

?>



</div>



<div id="previous-wrap">

<?php 

previous_posts_link("<< Prev");

?>

</div><div id="next-wrap">

<?php

next_posts_link("Next >>");

?>

</div>

</nav>



<?php

$wp_query = $temp_wp_query;

wp_reset_postdata();

get_footer();

?>