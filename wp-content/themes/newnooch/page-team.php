<?php
/*
Template Name: Team
*/
?>

<?php get_header(); ?>
<div class="team-wrap page-baseline">
	<div class="head-wrap about-page">
		<h1 class="section-header team-header mobile-only">Team Nooch</h1>
		<h1 class="section-header team-header tab-desk-only">About Nooch</h1>
		<nav class="about-subnav tab-desk-only">
			<ul>
				<li><a href="<?php echo get_page_link(1825); ?>" class="sub-nav-links">Overview</a></li>
				<li><a href="<?php echo get_page_link(1823); ?>" class="sub-nav-links">How It Works</a></li>
				<li><a href="<?php echo get_page_link(1826); ?>" class="sub-nav-links">Security</a></li>
				<li><a href="" class="sub-nav-links selected">Team</a></li>
			</ul>
		</nav>
	</div>

<?php
$page_args = array("page_id" => 1827 );
$page_query = new WP_Query( $page_args );

while ( $page_query->have_posts() ) : $page_query->the_post();
	$page_id = get_the_ID();
	$page_image = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'single-post-thumbnail' );
?>
	<ul id="team-buckets" style="background-image: url('<?php echo $page_image[0]; ?>')">
<?php
endwhile;
?>
		<?php 
		$counter = 1;
		
		function isOdd($i) {
			if ( $i % 2 == 0 ) {
				return false;
			} else {
				return true;
			}
		}

		$team_bios = new WP_Query( array( "post_type" => "team_bio", 'posts_per_page' => -1 ) );
		while ( $team_bios->have_posts() ) : $team_bios->the_post(); 

		 	$left = false;
		 	$right = false;
			$team_bio_id = $post->ID;
			$team_bio_title = get_post_meta( $team_bio_id, "job_title", true);
			$team_bio_fname = get_post_meta( $team_bio_id, "first_name", true);
			$team_bio_fbook = get_post_meta( $team_bio_id, "facebook_link", true);
			$team_bio_twit = get_post_meta( $team_bio_id, "twitter_link", true);
			$team_bio_linkd = get_post_meta( $team_bio_id, "linked_link", true);
			
			if (isOdd($counter)) {
				$left_right = "pic-left";
				$left = true;
			} else {
				$left_right = "pic-right";
				$right = true;
			}

			?>

			<li id="<?php echo $team_bio_fname; ?>" class="<?php echo $left_right ?>"><?php 
				if ( $left ) {
					if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'team-img-size' );
					}
				} 
				?><article class='bio-wrap'>
					<h1><?php the_title() ?></h1>
					<h2><?php echo $team_bio_title ?></h2>
					<div class="ind-social-wrap"><?php
						if ( $team_bio_fbook ){ ?>
							<a href="<?php echo $team_bio_fbook ?>">
								<img height="25" width="25" src="<?php echo get_template_directory_uri(); ?>/library/images/contactus-fb.png"></a><?php
						} if ( $team_bio_twit ){ ?>
							<a href="<?php echo $team_bio_twit ?>">
								<img height="25" width="25" src="<?php echo get_template_directory_uri(); ?>/library/images/contactus-twit.png"></a><?php
						} if ( $team_bio_linkd ){ ?>
							<a href="<?php echo $team_bio_linkd ?>">
								<img height="25" width="25" src="<?php echo get_template_directory_uri(); ?>/library/images/contactus-linkd.png"></a><?php
						} 
					?></div>
				<?php the_content(); ?>
				</article><?php
				if ( $right ) {
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'team-img-size' );
					} 
				}
			?></li>

			<?php $counter++ ?>
	<?php endwhile; ?>			
	</ul>
</div>
<?php get_footer(); ?>
